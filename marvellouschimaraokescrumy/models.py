from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class GoalStatus(models.Model):
    status_name = models.CharField(max_length=200)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    user = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)
    goal_name = models.CharField(max_length=200)
    goal_id = models.IntegerField(unique=True)
    created_by = models.CharField(max_length=200)
    moved_by = models.CharField(max_length=200)
    owner = models.CharField(max_length=200)
    goal_status= models.ForeignKey(GoalStatus, on_delete=models.PROTECT)

    def __str__(self):
        return self.goal_name


class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=200)
    created_by = models.CharField(max_length=200)
    moved_from = models.CharField(max_length=200)
    moved_to = models.CharField(max_length=200)
    time_of_action = models.DateTimeField(auto_now_add=True)
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by


