from django.urls import path, include
from . import views

app_name='myapp'
urlpatterns = [
    path('', views.index, include('marvellouschimaraokescrumy.urls', namespace='marvellouschimaraokescrumy')),
    path('movegoal/<int:goal_id>/', views.move_goal),
    path('addgoal/', views.add_goal),
    path('home/', views.home),
    path('accounts/', include('django.contrib.auth.urls')),
]
