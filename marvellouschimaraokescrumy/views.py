from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User

from .models import (GoalStatus, ScrumyGoals, ScrumyHistory)
import random
# Create your views here.

# def get_grading_parameters(request):
#     return HttpResponse('Welcome to Django')

def index(request):
    # return HttpResponse('This is a Scrum Application')
    goal = ScrumyGoals.objects.filter(goal_name='Learn Django')
    return HttpResponse(goal)


def move_goal(request, goal_id):
    # ScrumyGoals.objects.get(goal_id=goal_id)
    # goal = ScrumyGoals.objects.get(goal_name='Learn Django')
    # return HttpResponse(goal)
    try: 
        obj = ScrumyGoals.objects.get(goal_id=goal_id) 
    except Exception as e: 
        return render(
            request, 
            'marvellouschimaraokescrumy/exception.html', 
            {'error': 'A record with that goal id does not exist'}
        )
    else: 
        return HttpResponse(obj.goal_name)


def add_goal(request):
    ScrumyGoals.objects.create(
        user=User.objects.get(username='LouisOma'),
        goal_name='Keep Learning Django',
        goal_id= random.randint(1000,9999),
        created_by = 'Louis',
        moved_by='Louis',
        owner='Louis',
        goal_status=GoalStatus.objects.get(status_name='Weekly Goal')
    )


def home(request):
    result = ScrumyGoals.objects.get(goal_name='Learn Django')
    to_display = {
        'goal_name': result.goal_name,
        'goal_id': result.goal_id,
        'user': result.user.first_name
    }
    return render(request, 'marvellouschimaraokescrumy/home.html', to_display)
    
    


